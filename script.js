        //Массив тудушек. Проверяет, лежит ли что-нибудь в Storage. Если да, берет тудушку оттуда, иначе - присваивается пустой массив
        let todos = window.localStorage.getItem('todos') ? JSON.parse(window.localStorage.getItem('todos')) : []
        //Обработчик добавления тудушки
        const addHandler = () => {
            //Создание новой тудушки. Значение берется из поля. Id генерируется из текущей даты. Состояние по умолчанию "Не выполнено"
            let value = document.getElementById('input').value
            //Нет пустым тудушкам!
            if (!value) return
            let date = new Date()
            let id = `${date.getMonth()}-${date.getDay()}-${date.getTime()}`
            let state = false
            //Отрисовка новой тудушки
            renderTodo(value, id, state)
            //Добавление ее в массив
            todos.push({
                value,
                id,
                state
            })
            //Обновление storage
            window.localStorage.setItem('todos', JSON.stringify(todos))
            //Обнуление поля
            document.getElementById('input').value = ''
        }
        //Обработчик удаления тудушки
        const deleteHandler = (id) => {
            //Удаление тудушки из списка
            document.getElementById(id).remove()
            //Поиск тудушки в массиве по id и удаление ее из него
            for(let i = 0; i < todos.length; i++){
                if(todos[i].id === id) {
                    todos.splice(i,1)
                    //Обновление storage
                    window.localStorage.setItem('todos', JSON.stringify(todos))
                    return
                }
            }
        }
        //Обработчик изменения состояния
        const completedHandler = (id) => {
            //Поиск тудушки по ид в списке и изменение класса на "completed" (или удаление этого класса)
            let li = document.getElementById(id)
            li.classList.toggle('completed')
            //Поиск тудушки в массиве, изменение ее состояния в массиве и storage
            for(let i = 0; i < todos.length; i++){
            if(todos[i].id === id) {
                    todos[i].state = !todos[i].state
                    window.localStorage.setItem('todos', JSON.stringify(todos))
                    return
                }
            }
        } 

        //Функция для отрисовки одной тудушки
        const renderTodo = (value, id, state) => {
            //Создаю элемент списка и присваиваю ему переданный id
            let li = document.createElement('div')
            let div = document.createElement('div')
            let span = document.createElement('span')
            li.id = id
            //Добавляю кнопку удаления
            let deleteButton = document.createElement('button')
            deleteButton.innerHTML = 'X'
            deleteButton.onclick = () => deleteHandler(id) //Вызываю обработчик удаления
            //Добавляю галочку "Выполнено"/"Не выполнено"
            let completed = document.createElement('input')
            completed.type = 'checkbox'
            completed.onchange = () => completedHandler(id) //Вызываю обработчик
            //Проверяю статус тудушки, чтобы знать, как ее отрисовать
            if (state) {
                li.classList.toggle('completed')
                completed.checked = true
            }
            //Добавляю к списку полученную тудушку
            div.appendChild(completed)
            span.appendChild(document.createTextNode(value))
            div.appendChild(span)
            li.appendChild(div)
            li.appendChild(deleteButton)
            document.getElementById('todos').appendChild(li)
        }

        //Функция для отрисовки всего списка
        const fetchTodos = () => {
            //Получаю статус фильтров
            let showCompleted = document.getElementsByName('completed')[0].checked
            let showIncompleted = document.getElementsByName('incompleted')[0].checked

            //Очищаю список
            document.getElementById('todos').innerHTML = ''

            //Наполняю список подходящими по фильрам тудушками
            todos.map(todo => {
                if ((showCompleted && todo.state) || (showIncompleted && !todo.state))
                renderTodo(todo.value, todo.id, todo.state)
            }
        )
    }